const fs=require('fs')
const botmsg=require('./botmsg.json')

let lastWarningReset=new Date().getTime()

function addGuild(id, guilds, autorep, kick){
    guilds[id]={lang: 'en', prefix: '/'}
    kick.kicked[id]={}
    autorep[id]={}
    fs.writeFile('./data/guilds.json', JSON.stringify(guilds), err=>{return err ? err :  console.log('New guild added')})
    fs.writeFile('./data/kick.json', JSON.stringify(kick), err=>{return err ? err :  console.log('New guild added')})
    fs.writeFile('./data/autorep.json', JSON.stringify(autorep), err=>{return err ? err :  console.log('wow')})
}

function autorep(msg, guildid, autorep){
    if(autorep[guildid][msg.content.toLowerCase()]){
        msg.isCmd=true
        msg.channel.send(autorep[guildid][msg.content.toLowerCase()])
    }
}

function help(guilds, guildid, cmd){
    let str=botmsg[guilds[guildid].lang].guildprefix+guilds[guildid].prefix+'\n'
    str+=botmsg[guilds[guildid].lang].cmdlist+Object.keys(cmd).toString().replace(/,/g, ', ')
    return str
}

function getDatetimeStr(date){
    return date.getDate()+'/'+(date.getMonth()+1)+'/'+date.getFullYear()+' '+date.getHours()+':'+date.getMinutes()+':'+date.getSeconds()
}

function getUserRoles(guild, id){
    roles=[]
    if(guild.members.get(id).roles){
        guild.members.get(id).roles.forEach(role=>roles.push(role.id))
    }
    return roles
}

function removeMentions(guild, str){
    str=str.replace(/@everyone/g, '@ everyone')
    str=str.replace(/@here/g, '@ here')
    str=str.replace(/ +\|/, '|')
    let mentions=str.match(/<@&{0,1}!{0,1}\d{18}>/g)
    if(mentions){
        mentions.forEach(men=>{
            let id=men.match(/\d{18}/)[0]
            if(men.match('&')){
                str=(guild.roles.get(id) ? str.replace(men, '@'+guild.roles.get(id).name) : str)
            }else{
                str=(guild.members.get(id) ? str.replace(men, '@'+guild.members.get(id).user.username) : str)
            }
        })
    }
    return str
}


module.exports={
    addGuild: addGuild,
    autorep: autorep,
    help: help,
    getDatetimeStr: getDatetimeStr,
    getUserRoles: getUserRoles,
    removeMentions: removeMentions
}
