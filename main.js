const discord=require('discord.js')
const client=new discord.Client()

const conf=require('./config.json')
const guilds=require('./data/guilds.json')
const ignored=require('./data/ignored.json')
const autorep=require('./data/autorep.json')
const kick=require('./data/kick.json')
const cmd=require('./src/cmd.js')
const misc=require('./src/misc.js')

let startTimestamp=0


client.on('ready', ()=>{
    startTimestamp=new Date().getTime()/1000|0
    client.user.setActivity(conf.playing)
    console.log(conf.playing)
})

function sanitizeMessage(msg) {
    var dict = ["ÀÁÂÃÄÅẤẮẲẴẶẦẰȂĀĂĄǍǺA̋ȀA̧𝐀𝐴𝑨𝖠𝗔𝘈𝘼𝒜𝓐𝔄𝕬𝔸𝙰Ⓐ🄐🅐🄰🅰Ａᴀ🇦ⒶΑ₳ᴀᎪǞᗩÄḀＡ卂ĂᏗⱯΔคⒶᏜᎯĄДⱭꍏᴬΛƛᕱԹﾑ","ÆǼ","ÇḈĆĈĊČC̆Č̣𝐂𝐶𝑪𝖢𝗖𝘊𝘾𝒞𝓒ℭ𝕮ℂ𝙲Ⓒ🄒🅒🄲🅲Ｃᴄ🇨Ⓒ¢₵ᴄᏟƇᑕĊḈＣ匚ČፈƆĆ૮ΣⒸᏨℂϹꉓᶜㄈƇ꒝८Շᄃ","ÈÉÊËẾḖỀḔḜȆĒĔĖĘĚE̋ȄÊ̌ȨƐ̧𝐄𝐸𝑬𝖤𝗘𝘌𝙀ℰ𝓔𝔈𝕰𝔼𝙴Ⓔ🄔🅔🄴🅴Ｅᴇ🇪ⒺЄɆᴇᎬƐỆḔＥΕ乇ĔᏋƎ€ᗴⒺ℮ƎΞҼꍟᴱƐЄꂅ૯ȜĒ","ÌÍÎÏḮȊĨĪĬĮİǏI̋ȈI̧Ɨ̧𝐈𝐼𝑰𝖨𝗜𝘐𝙄ℐ𝓘ℑ𝕴𝕀𝙸Ⓘ🄘🅘🄸🅸ＩꞮ🇮ⒾΙŁꞮᎥÍƗÏḬＩ丨ĨIƗเⒾℹÎꀤᴵƖɿﾉ","ÐĎĐḐ𝐃𝐷𝑫𝖣𝗗𝘋𝘿𝒟𝓓𝔇𝕯𝔻𝙳Ⓓ🄓🅓🄳🅳Ｄᴅ🇩Ⓓ∂ĐᴅᎠƉᗪĎḊＤĎᎴ๔ⒹᗬᕍԺꀸᴰÐƊძԺり໓","ÑŃŅŇN̆Ǹ𝐍𝑁𝑵𝖭𝗡𝘕𝙉𝒩𝓝𝔑𝕹ℕ𝙽Ⓝ🄝🅝🄽🅽Ｎɴ🇳ⓃИ₦ɴᏁՌᑎŃṆＮΗ几ŃŇɳภⓃƝŊИՂꈤᴺЛƝՈՌ刀ຖ","ÒÓÔÕÖØỐṌṒȎŌŎŐƠǑǾỒṐȌO̧𝐎𝑂𝑶𝖮𝗢𝘖𝙊𝒪𝓞𝔒𝕺𝕆𝙾Ⓞ🄞🅞🄾🅾Ｏᴏ🇴ⓄΣØᴏᎾՕÖṎＯㄖŐᎧᗝ๏ⓄÑƠØФꂦᴼƠԾ૦の⊕໐","ÙÚÛÜŨŪŬŮŰŲȖƯǓǕǗǙǛỨṸỪȔU̧𝐔𝑈𝑼𝖴𝗨𝘜𝙐𝒰𝓤𝔘𝖀𝕌𝚄Ⓤ🄤🅤🅄🆄Ｕᴜ🇺ⓊΥɄᴜƱᑌÜṲＵㄩÚᏬỮΜยⓊƮŲЦՄꀎᵁƲՄひน","ÝŶŸY̆ỲY̌𝐘𝑌𝒀𝖸𝗬𝘠𝙔𝒴𝓨𝔜𝖄𝕐𝚈Ⓨ🄨🅨🅈🆈Ｙʏ🇾ⓎУɎʏᎽЧŸẎＹҮㄚŶᎩʎ¥ყƳⓎℵჯУՎꌩᵞϤՎﾘΨฯ","àáâãäåấắẳẵặầằȃāăąǎǻa̋ȁa̧𝐚𝑎𝒂𝖺𝗮𝘢𝙖𝒶𝓪𝔞𝖆𝕒𝚊ⓐ⒜ᵃⓐα₳ᴀꭺǟᗩäḁａ卂ăꮧɐδคⓐꮬꭿąдɑꍏᴬλƛᕱթﾑ","æǽ","çḉćĉċčc̆č̣𝐜𝑐𝒄𝖼𝗰𝘤𝙘𝒸𝓬𝔠𝖈𝕔𝚌ⓒ⒞ᶜⓒ¢₵ᴄꮯƈᑕċḉｃ匚čፈɔć૮ςⓒꮸℂϲꉓᶜㄈƈ꒝८շᄃ","èéêëếḗềḕḝȇēĕėęěe̋ȅê̌ȩɛ̧𝐞𝑒𝒆𝖾𝗲𝘦𝙚ℯ𝓮𝔢𝖊𝕖𝚎ⓔ⒠ᵉⓔєɇᴇꭼɛệḕｅε乇ĕꮛǝ€ᗴⓔ℮ǝξҽꍟᴱɛєꂅ૯ȝē","ìíîïḯȋĩīĭįıǐi̋ȉi̧ɨ̧𝐢𝑖𝒊𝗂𝗶𝘪𝙞𝒾𝓲𝔦𝖎𝕚𝚒ⓘ⒤ⁱⓘιłɪꭵíɨïḭｉ丨ĩıɨเⓘℹîꀤᴵɩɿﾉ!","ðďđḑ𝐝𝑑𝒅𝖽𝗱𝘥𝙙𝒹𝓭𝔡𝖉𝕕𝚍ⓓ⒟ᵈⓓ∂đᴅꭰɖᗪďḋｄďꮄ๔ⓓᗬᕍժꀸᴰðɗძժり໓","ñńņňŉn̆ǹ𝐧𝑛𝒏𝗇𝗻𝘯𝙣𝓃𝓷𝔫𝖓𝕟𝚗ⓝ⒩ⁿⓝи₦ɴꮑռᑎńṇｎη几ńňɳภⓝɲŋиղꈤᴺлɲոռ刀ຖ","òóôõöøốṍṓȏōŏőơǒǿồṑȍo̧𝐨𝑜𝒐𝗈𝗼𝘰𝙤ℴ𝓸𝔬𝖔𝕠𝚘ⓞ⒪ᵒⓞσøᴏꮎօöṏｏㄖőꭷᗝ๏ⓞñơøфꂦᴼơծ૦の⊕໐","ùúûüũūŭůűųȗưǔǖǘǚǜứṹừȕu̧𝐮𝑢𝒖𝗎𝘂𝘶𝙪𝓊𝓾𝔲𝖚𝕦𝚞ⓤ⒰ᵘⓤυʉᴜʊᑌüṳｕㄩúꮼữµยⓤʈųцմꀎᵁʋմひน","ýÿŷy̆ỳy̌𝐲𝑦𝒚𝗒𝘆𝘺𝙮𝓎𝔂𝔶𝖞𝕪𝚢ⓨ⒴ʸⓨуɏʏꮍчÿẏｙүㄚŷꭹʎ¥ყƴⓨℵჯуվꌩᵞϥվﾘψฯ","ĜǴĞĠĢǦ𝐆𝐺𝑮𝖦𝗚𝘎𝙂𝒢𝓖𝔊𝕲𝔾𝙶Ⓖ🄖🅖🄶🅶Ｇɢ🇬Ⓖ₲ɢᎶĠḠＧĞƂǤƓﻮⒼℊՑꁅᴳƓ૭Գムງ","ĝǵğġģǧ𝐠𝑔𝒈𝗀𝗴𝘨𝙜ℊ𝓰𝔤𝖌𝕘𝚐ⓖ⒢ᵍⓖ₲ɢꮆġḡｇğƃǥɠﻮⓖℊցꁅᴳɠ૭գムງ","ĤĦḪȞḨ𝐇𝐻𝑯𝖧𝗛𝘏𝙃ℋ𝓗ℌ𝕳ℍ𝙷Ⓗ🄗🅗🄷🅷Ｈʜ🇭ⒽНⱧʜꞪᕼḦＨ卄ĤᏂꞍĦЂⒽዞᏲɧℍՀꃅᴴӇ♅Һん","ĥħḫȟḩ𝐡ℎ𝒉𝗁𝗵𝘩𝙝𝒽𝓱𝔥𝖍𝕙𝚑ⓗ⒣ʰⓗнⱨʜɦᕼḧｈ卄ĥꮒɥħђⓗዞᏺɧℍհꃅᴴӈ♅һん","Ĳ","ĳ","ĴJ̌𝐉𝐽𝑱𝖩𝗝𝘑𝙅𝒥𝓙𝔍𝕵𝕁𝙹Ⓙ🄙🅙🄹🅹Ｊᴊ🇯ⒿנᴊꞲᒍＪﾌĴᏠɾᒎןⒿᎫᏧՅꀭᴶʆͿว","ĵǰ𝐣𝑗𝒋𝗃𝗷𝘫𝙟𝒿𝓳𝔧𝖏𝕛𝚓ⓙ⒥ʲⓙנᴊʝᒍｊﾌĵꮰɾᒎןⓙꭻꮷյꀭᴶʆϳว","ĶḰK̆Ǩ𝐊𝐾𝑲𝖪𝗞𝘒𝙆𝒦𝓚𝔎𝕶𝕂𝙺Ⓚ🄚🅚🄺🅺Ｋᴋ🇰ⓀК₭ᴋᏦḲḲＫҜĶꞰҠᛕĸⓀƘƘꀘᴷҚКҚズΚ","ķḱk̆ǩ𝐤𝑘𝒌𝗄𝗸𝘬𝙠𝓀𝓴𝔨𝖐𝕜𝚔ⓚ⒦ᵏⓚк₭ᴋꮶḳḳｋҝķʞҡᛕĸⓚƙƙꀘᴷқкқズκ","ĹĻĽĿ𝐋𝐿𝑳𝖫𝗟𝘓𝙇ℒ𝓛𝔏𝕷𝕃𝙻Ⓛ🄛🅛🄻🅻Ｌʟ🇱ⓁℓⱠʟᏞᒪĿḶＬㄥĹᏝŁⓁᎱӀ꒒ᴸԼԼՆʅﾚ","ĺļľŀłł𝐥𝑙𝒍𝗅𝗹𝘭𝙡𝓁𝓵𝔩𝖑𝕝𝚕ⓛ⒧ˡⓛℓⱡʟꮮᒪŀḷｌㄥĺꮭłⓛꮁӏ꒒ᴸլլնʅﾚ","ḾM̆M̌M̧𝐌𝑀𝑴𝖬𝗠𝘔𝙈ℳ𝓜𝔐𝕸𝕄𝙼Ⓜ🄜🅜🄼🅼Ｍᴍ🇲ⓂМ₥ᴍʍᗰṂṀＭ爪МᎷƜΜⱮ๓Ⓜℳꎭᴹ௱ოﾶ","ḿm̆m̌m̧𝐦𝑚𝒎𝗆𝗺𝘮𝙢𝓂𝓶𝔪𝖒𝕞𝚖ⓜ⒨ᵐⓜм₥ᴍʍᗰṃṁｍ爪мꮇɯμɱ๓ⓜℳꎭᴹ௱ოﾶ","Œ","œ","P̆ṔP̌𝐏𝑃𝑷𝖯𝗣𝘗𝙋𝒫𝓟𝔓𝕻ℙ𝙿Ⓟ🄟🅟🄿🅿Ｐᴘ🇵ⓅΡ₱ᴘᏢՔᑭṖṖＰ卩РᎮƤקⓅ⋆℘ᖘᴾÞᎵǷｱ","p̆ṕp̌𝐩𝑝𝒑𝗉𝗽𝘱𝙥𝓅𝓹𝔭𝖕𝕡𝚙ⓟ⒫ᵖⓟρ₱ᴘꮲքᑭṗṗｐ卩рꭾƥקⓟ⋆℘ᖘᴾþꮅƿｱ","ŔŖŘR̆ȒȐŘ̩Ř̩𝐑𝑅𝑹𝖱𝗥𝘙𝙍ℛ𝓡ℜ𝕽ℝ𝚁Ⓡ🄡🅡🅁🆁ＲƦ🇷ⓇЯⱤƦᏒᖇŔṘＲ尺ŔɹŘ૨ГⓇɸཞЯꋪᴿƦՐ","ŕŗřr̩𝐫𝑟𝒓𝗋𝗿𝘳𝙧𝓇𝓻𝔯𝖗𝕣𝚛ⓡ⒭ʳⓡяɽʀꮢᖇŕṙｒ尺ŕɹř૨гⓡɸཞяꋪᴿʀր","ŚŜŞȘŠṤṦ𝐒𝑆𝑺𝖲𝗦𝘚𝙎𝒮𝓢𝔖𝕾𝕊𝚂Ⓢ🄢🅢🅂🆂Ｓꜱ🇸ⓈЅ₴ՖᔕṨṠＳᴤ丂ŚᏕŞรⓈℛʂƧՏꌗˢらŠ","śŝșşšſṥṧ𝐬𝑠𝒔𝗌𝘀𝘴𝙨𝓈𝓼𝔰𝖘𝕤𝚜ⓢ⒮ˢⓢѕ₴ֆᔕṩṡｓᴤ丂śꮥşรⓢℛʂƨտꌗˢらš","ŢȚŤŦT̆𝐓𝑇𝑻𝖳𝗧𝘛𝙏𝒯𝓣𝔗𝕿𝕋𝚃Ⓣ🄣🅣🅃🆃Ｔᴛ🇹ⓉТ₮ᴛᏆT̈ṮＴㄒŤᏖꞱƬ丅ⓉຮꞭ✞ΓԵ꓄ᵀƬϮｲ†","ţțťŧt̆𝐭𝑡𝒕𝗍𝘁𝘵𝙩𝓉𝓽𝔱𝖙𝕥𝚝ⓣ⒯ᵗⓣт₮ᴛꮖẗṯｔㄒťꮦʇƭ丅ⓣຮɬ✞γե꓄ᵀƭϯｲ†","V̆V̌𝐕𝑉𝑽𝖵𝗩𝘝𝙑𝒱𝓥𝔙𝖁𝕍𝚅Ⓥ🄥🅥🅅🆅Ｖᴠ🇻ⓋΝᴠᏉƲᐯṾṼＶɅѴשⓋɄᙈ۷ΘᵛƔע√∀ง","v̆v̌𝐯𝑣𝒗𝗏𝘃𝘷𝙫𝓋𝓿𝔳𝖛𝕧𝚟ⓥ⒱ᵛⓥνᴠꮙʋᐯṿṽｖʌѵשⓥʉᙈ۷ϑᵛɣע√∀ง","ŴẂẀW̌𝐖𝑊𝑾𝖶𝗪𝘞𝙒𝒲𝓦𝔚𝖂𝕎𝚆Ⓦ🄦🅦🅆🆆Ｗᴡ🇼ⓌΩ₩ᴡᎳԱᗯẄẆＷ山ŴᏇฬⓌΩΙᏔЩꅏᵂƜຟ","ŵẃẁw̌𝐰𝑤𝒘𝗐𝘄𝘸𝙬𝓌𝔀𝔴𝖜𝕨𝚠ⓦ⒲ʷⓦω₩ᴡꮃաᗯẅẇｗ山ŵꮗฬⓦῳꮤщꅏᵂɯຟ","X̆X́X̌X̧𝐗𝑋𝑿𝖷𝗫𝘟𝙓𝒳𝓧𝔛𝖃𝕏𝚇Ⓧ🄧🅧🅇🆇Ｘ🇽ⓍΧӾХ᙭ẌẌＸᴥ乂ЖጀאץⓍҲ✘×ꊼˣҲՃﾒ","x̆x́x̌x̧𝐱𝑥𝒙𝗑𝘅𝘹𝙭𝓍𝔁𝔵𝖝𝕩𝚡ⓧ⒳ˣⓧχӿх᙭ẍẍｘᴥ乂жጀאץⓧҳ✘×ꊼˣҳճﾒ","ŹŻŽZ̧𝐙𝑍𝒁𝖹𝗭𝘡𝙕𝒵𝓩ℨ𝖅ℤ𝚉Ⓩ🄩🅩🅉🆉Ｚᴢ🇿ⓏⱫᴢᏃʐᘔẒẒＺ乙ŹፚŽƵⓏY̊ʑՀꁴᶻẔȤຊ","źżžz̧𝐳𝑧𝒛𝗓𝘇𝘻𝙯𝓏𝔃𝔷𝖟𝕫𝚣ⓩ⒵ᶻⓩⱬᴢꮓʐᘔẓẓｚ乙źፚžƶⓩẙʑհꁴᶻẕȥຊ","ƒf̌𝐟𝑓𝒇𝖿𝗳𝘧𝙛𝒻𝓯𝔣𝖋𝕗𝚏ⓕ⒡ᶠⓕ₣ғʄᖴḟｆᴈ千ŧꭶɟƒⓕℱﾓꎇᶠƒꊰբｷ","Þ","þ","Ѓ","ѓ","Ќ","ќ","B̌B̧𝐁𝐵𝑩𝖡𝗕𝘉𝘽ℬ𝓑𝔅𝕭𝔹𝙱Ⓑ🄑🅑🄱🅱Ｂʙ🇧ⒷВ฿ʙɮᗷḄḂＢ乃ΒᏰƁ๒ⒷℬცБҌꌃᴮϦƁՅ๖","b̌b̧𝐛𝑏𝒃𝖻𝗯𝘣𝙗𝒷𝓫𝔟𝖇𝕓𝚋ⓑ⒝ᵇⓑв฿ʙɮᗷḅḃｂ乃βᏸɓ๒ⓑℬცбҍꌃᴮϧɓյß๖","F̌𝐅𝐹𝑭𝖥𝗙𝘍𝙁ℱ𝓕𝔉𝕱𝔽𝙵Ⓕ🄕🅕🄵🅵Ｆꜰ🇫Ⓕ₣ҒʄᖴḞＦᴈ千ŦᎦɟƑⒻℱﾓꎇᶠƑꊰԲｷ","Q̌Q̧𝐐𝑄𝑸𝖰𝗤𝘘𝙌𝒬𝓠𝔔𝕼ℚ𝚀Ⓠ🄠🅠🅀🆀Ｑ🇶ⓆǪԶᑫＱᴓɊᎤΩΦỢⓆᑬǪꆰᵟҨƢԳҨゐ๑","q̌q̧𝐪𝑞𝒒𝗊𝗾𝘲𝙦𝓆𝓺𝔮𝖖𝕢𝚚ⓠ⒬ⓠǫզᑫｑᴓɋꭴωφợⓠᑬǫꆰᵟҩƣգҩゐ๑","𝟎𝟢𝟬𝟘𝟶⓪⓿０⁰₀","𝟏𝟣𝟭𝟙𝟷①❶⑴¹₁","𝟐𝟤𝟮𝟚𝟸②❷⑵²₂","𝟑𝟥𝟯𝟛𝟹③❸⑶³₃","𝟒𝟦𝟰𝟜𝟺④❹⑷⁴₄","𝟓𝟧𝟱𝟝𝟻⑤❺⑸⁵₅","𝟔𝟨𝟲𝟞𝟼⑥❻⑹⁶₆","𝟕𝟩𝟳𝟟𝟽⑦❼⑺⁷₇","𝟖𝟪𝟴𝟠𝟾⑧❽⑻⁸₈","𝟗𝟫𝟵𝟡𝟿⑨❾⑼⁹₉"];
    var dict2 = ["A","AE","C","E","I","D","N","O","U","Y","a","ae","c","e","i","d","n","o","u","y","G","g","H","h","IJ","ij","J","j","K","k","L","l","M","m","OE","oe","P","p","R","r","S","s","T","t","V","v","W","w","X","x","Z","z","f","TH","th","Г","г","К","к","B","b","F","Q","q","0","1","2","3","4","5","6","7","8","9"];
    for(i = 0; i<dict.length; i++){
        msg.replace(new RegExp(dict[i], 'g'), dict2[i]);
    }
    return msg;
}

client.on('message', msg=>{
    if(msg.channel.type=='text' && msg.author.bot==false){
        msg.content = sanitizeMessage(msg.content);
        let guildid='a'+msg.guild.id  // 'a' because variable names can't start with a number

        if(!guilds[guildid]||!autorep[guildid]){ // If the bot doesn't know the server the message is from
            misc.addGuild(guildid, guilds, autorep, kick)
        }

        if(!ignored.includes(msg.channel.id)){

            if(msg.content===conf.helpcmd)msg.channel.send(misc.help(guilds, guildid, cmd))

            if(msg.content.startsWith(guilds[guildid].prefix)){  // command parser
                let command=msg.content.substr(guilds[guildid].prefix.length)
                command=msg.content.match(/[A-zÀ-ÿ]+/)[0]
                let arg=msg.content.substr(msg.content.match(/[A-zÀ-ÿ]+/)[0].length+2)
                msg.isCmd=true
                switch(command){
                    case 'coconérateur':
                        msg.channel.send(cmd.coconerateur())
                        break
                    //case 'setprefix':
                        //msg.channel.send(cmd.setprefix(arg, guilds, guildid))
                        //break
                    case 'setlanguage':
                        msg.channel.send(cmd.setlanguage(arg, guilds, guildid))
                        break
                    case 'stfu':
                        msg.channel.send(cmd.stfu(ignored, msg.channel.id, guilds, guildid))
                        break
                    case 'uptime':
                        msg.channel.send(cmd.uptime(startTimestamp))
                        break
                    case 'addresponse':
                        msg.channel.send(cmd.addresponse(arg, autorep, guilds, guildid, msg.guild, misc.removeMentions))
                        break
                    case 'delresponse':
                        msg.channel.send(cmd.delresponse(arg, autorep, guilds, guildid))
                        break
                    case 'delall':
                        msg.channel.send(cmd.delall(msg.member, autorep, guilds, guildid))
                        break
                    case 'votekick':
                        msg.channel.send(cmd.votekick(msg,guilds, guildid, conf.votekick.time, conf.votekick.count, misc.getUserRoles))
                        break
                    default:
                        msg.isCmd=false
                }
            }
            misc.autorep(msg, guildid, autorep)
        }else{
            if(msg.content===guilds[guildid].prefix+'talk'){
                msg.channel.send(cmd.stfu(ignored, msg.channel.id, guilds, guildid))
            }
        }
    }
})

client.on('messageDelete', msg=>{
    if(msg.channel.type==='text' && msg.author.bot===false){
        if(msg.isCmd && !ignored.includes(msg.channel.id)){
            msg.content=misc.removeMentions(msg.guild, msg.content)
            msg.channel.send('**"'+msg.content+'"**\n    *<@'+msg.author.id+'>, '+misc.getDatetimeStr(msg.createdAt)+'*')
        }
    }
})

client.on('messageUpdate', (oldmsg, msg)=>{
    if(msg.channel.type==='text' && msg.author.bot===false){
        if(msg.isCmd && !ignored.includes(msg.channel.id)){
            msg.content=oldmsg.content
            msg.channel.send('**"'+msg.content+'"**\n    *<@'+msg.author.id+'>, '+misc.getDatetimeStr(msg.createdAt)+'*')
        }
    }
})


client.on('guildMemberAdd', member=>{
    setTimeout(()=>{
        if(kick.kicked['a'+member.guild.id][member.id]){
            kick.kicked['a'+member.guild.id][member.id].forEach(role=>member.addRole(role))
            delete kick.kicked['a'+member.guild.id][member.id]
        }
    }, conf.votekick.roleTimeout*1000)
})

client.on('messageReactionAdd', (reac, user)=>{
    if(conf.selfReactionKick.includes(reac.emoji.id) && user.id === reac.message.author.id){
        reac.message.member.kick()
    }
})

client.on('error', err=>{
    client.login(conf.token)
    if(conf.errorChannel) client.channels.get(conf.errorChannel).send(err.message)
})


client.login(conf.token)
